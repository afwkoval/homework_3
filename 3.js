let array = [4,7,6,1,5,2,8,2,2,9,0];
function sortArray(array) {
    const odd = array.filter((x) => x % 2).sort((a,b) => a - b);
    return array.map((x) => x % 2 ? odd.shift() : x);
 }

 console.log(sortArray(array));