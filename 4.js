function anagrams(word, words) {
    return words.filter(function(item){
      return item.split('').sort().join('') === word.split('').sort().join('');
    });
  }
console.log(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']));
console.log(anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']));
console.log(anagrams('laser', ['lazing', 'lazy',  'lacer']));
